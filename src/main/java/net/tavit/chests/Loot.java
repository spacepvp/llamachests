package net.tavit.chests;

import lombok.Getter;
import net.tavit.chests.Utilities.utils;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Loot {

    @Getter
    private ItemStack lootItem;

    @Getter
    private int min, max;

    @Getter
    private List<String> lootCommands;

    @Getter
    private double lootChance;

    @Getter
    private ItemStack handItem;

    @Getter
    private String handHolo;

    @Getter
    private boolean onlyCommand;

    public Loot setLootItem(ItemStack item) {
        this.lootItem = item;
        return this;
    }

    public Loot setMin(int min) {
        this.min = min;
        return this;
    }

    public Loot setMax(int max) {
        this.max = max;
        return this;
    }

    public Loot setCommands(List<String> commands) {
        this.lootCommands = commands;
        return this;
    }

    public Loot setLootChance(double chance) {
        this.lootChance = chance;
        return this;
    }

    public Loot setHandItem(ItemStack item) {
        this.handItem = item;
        return this;
    }

    public Loot setHandHolo(String holo) {
        this.handHolo = holo;
        return this;
    }

    public Loot setIsOnlyCommand(boolean b) {
        this.onlyCommand = b;
        return this;
    }

    public Loot build() {
        Loot loot = new Loot();
        loot.lootItem = this.lootItem;
        loot.min = this.min;
        loot.max = this.max;
        loot.lootCommands = this.lootCommands;
        loot.lootChance = this.lootChance;
        loot.handItem = this.handItem;
        loot.handHolo = this.handHolo;
        loot.onlyCommand = this.onlyCommand;
        return loot;
    }

    public ItemStack getFinalChosenItem() {
        int amount = utils.getRandomNumber(min, max);
        ItemStack item = lootItem;
        item.setAmount(amount);
        return item;
    }

}
