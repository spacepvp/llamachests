package net.tavit.chests.Files;

import net.tavit.chests.LlamaChests;

public class ChestFile extends FileHandler {
    public ChestFile(LlamaChests llamaChests) {
        super(llamaChests, "chests.yml");
    }
}
