package net.tavit.chests.Files;

import net.tavit.chests.LlamaChests;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileHandler {

    protected LlamaChests main;
    private File file;
    public FileConfiguration config;

    /**
     * @param main plugin instance
     * @param filename name of file
     */
    public FileHandler(LlamaChests main, String filename) {
        this.main = main;
        this.file = new File(main.getDataFolder(), filename);
        // check if file doesnt exist
        if (!this.file.exists()) {
            // try to create new file
            try {
                this.file.createNewFile();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        // load yaml configuration
        this.config = YamlConfiguration.loadConfiguration(this.file);
    }

    public void save() {
        // try to save file
        try {
            this.config.save(this.file);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        this.config = YamlConfiguration.loadConfiguration(this.file);
    }

}
