package net.tavit.chests;

import lombok.Getter;
import net.tavit.chests.Commands.CrateCommand;
import net.tavit.chests.Enums.AnimationSpeed;
import net.tavit.chests.Events.CrateEvents;
import net.tavit.chests.Files.ChestFile;
import net.tavit.chests.Utilities.Logger;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class LlamaChests extends JavaPlugin {

    @Getter
    private static LlamaChests instance;

    /*
     * files
     */
    @Getter
    private ChestFile chestFile;

    @Override
    public void onEnable() {
        instance = this;

        registerFiles();
        loadCommands();
        loadCrates();
        registerEvents();

        Logger.log("&aPlugin has been enabled!");
    }

    @Override
    public void onDisable() {
        saveCrates();
        Logger.log("&aPlugin has been disabled!");
    }

    /*
     * register plugin files
     */
    private void registerFiles() {
        saveDefaultConfig();
        chestFile = new ChestFile(this);
        // create chests section if it doesnt exist
        if(chestFile.config.getConfigurationSection("chests") == null) {
            chestFile.config.createSection("chests");
            chestFile.save();
        }
    }

    /*
     * register plugin events
     */
    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new CrateEvents(), this);
        getServer().getPluginManager().registerEvents(new CrateOpening(), this);
    }

    /*
     * load plugin commands
     */
    private void loadCommands() {
        getCommand("crate").setExecutor(new CrateCommand());
    }

    /*
     * load llama chests
     */
    private void loadCrates() {
        if(chestFile.config.getConfigurationSection("chests") == null) return;
        for(String chests : chestFile.config.getConfigurationSection("chests").getKeys(false)) {
            String chestName = chestFile.config.getString("chests." + chests + ".chest-name");
            ItemStack item = chestFile.config.getItemStack("chests." + chests + ".chest-item");
            String crateItemName = chestFile.config.getString("chests." + chests + ".chest-item-name");
            ArrayList<String> crateItemLore = (ArrayList<String>) chestFile.config.getStringList("chests." + chests + ".chest-item-lore");
            ArrayList<String> crateWorlds = (ArrayList<String>) chestFile.config.getStringList("chests." + chests + ".chest-worlds");
            ArrayList<String> crateRegions = (ArrayList<String>) chestFile.config.getStringList("chests." + chests + ".chest-regions");
            int rewardAmount = chestFile.config.getInt("chests." + chests + ".chest-reward-amount");
            boolean glowing = chestFile.config.getBoolean("chests." + chests + ".chest-glow");
            int lootToSpawn = chestFile.config.getInt("chests." + chests + ".chest-loot-to-spawn");
            AnimationSpeed speed = AnimationSpeed.valueOf(chestFile.config.getString("chests." + chests + ".chest-animation-speed"));
            boolean allowsPvP = chestFile.config.getBoolean("chests." + chests + ".chest-allows-pvp");

            List<Loot> chestLoot = new ArrayList<>();
            if(chestFile.config.getConfigurationSection("chests." + chests + ".loot") != null) {
                for (String loot : chestFile.config.getConfigurationSection("chests." + chests + ".loot").getKeys(false)) {
                    ItemStack lootItem = chestFile.config.getItemStack("chests." + chests + ".loot." + loot + ".loot-item");
                    int min = chestFile.config.getInt("chests." + chests + ".loot." + loot + ".loot-item-min");
                    int max = chestFile.config.getInt("chests." + chests + ".loot." + loot + ".loot-item-max");
                    ArrayList<String> commands = (ArrayList<String>) chestFile.config.getStringList("chests." + chests + ".loot." + loot + ".loot-commands");
                    double chance = chestFile.config.getDouble("chests." + chests + ".loot." + loot + ".loot-chance");
                    ItemStack handItem = chestFile.config.getItemStack("chests." + chests + ".loot." + loot + ".loot-hand-item");
                    String holo = chestFile.config.getString("chests." + chests + ".loot." + loot + ".loot-holo");
                    boolean onlyCommand = chestFile.config.getBoolean("chests." + chests + ".loot." + loot + ".loot-only-command");
                    chestLoot.add(new Loot()
                            .setLootItem(lootItem)
                            .setMin(min)
                            .setMax(max)
                            .setCommands(commands)
                            .setLootChance(chance)
                            .setHandItem(handItem)
                            .setHandHolo(holo)
                            .setIsOnlyCommand(onlyCommand)
                            .build());
                }
            }

            Crate.crates.add(new Crate()
            .setName(chestName)
            .setCrateItem(item)
            .setCrateItemName(crateItemName)
            .setCrateItemLore(crateItemLore)
            .setRewardAmount(rewardAmount)
            .setGlowing(glowing)
            .setLoot(chestLoot)
            .setCrateWorlds(crateWorlds)
            .setCrateRegions(crateRegions)
            .setLootToSpawn(lootToSpawn)
            .setAnimationSpeed(speed)
            .setAllowsPvP(allowsPvP)
            .build());
        }
        Logger.log("&aLoaded all Crates! Total: " + Crate.crates.size());
    }

    /*
     * save llama chests
     */
    private void saveCrates() {
        if(Crate.crates.size() == 0) return;
        // reset chest yaml file
        chestFile.config.set("chests", "{}");
        chestFile.save();
        for(Crate crate : Crate.crates) {
            chestFile.config.createSection("chests." + crate.getName());
            String crateName = crate.getName();
            ItemStack crateItem = crate.getCrateItem();
            String crateItemName = crate.getCrateItemName();
            ArrayList<String> crateItemLore = crate.getCrateItemLore();
            ArrayList<String> crateWorlds = crate.getAllowedWorlds();
            ArrayList<String> crateRegions = crate.getAllowedRegions();
            int rewardAmount = crate.getRewardAmount();
            boolean glowing = crate.isGlowing();
            List<Loot> loot = crate.getCrateLoot();
            int lootToSpawn = crate.getLootToSpawn();
            AnimationSpeed speed = crate.getAnimationSpeed();
            boolean allowsPvP = crate.isAllowsPvP();
            chestFile.config.set("chests." + crateName + ".chest-name", crateName);
            chestFile.config.set("chests." + crateName + ".chest-item", crateItem);
            chestFile.config.set("chests." + crateName + ".chest-item-name", crateItemName);
            chestFile.config.set("chests." + crateName + ".chest-item-lore", crateItemLore);
            chestFile.config.set("chests." + crateName + ".chest-worlds", crateWorlds);
            chestFile.config.set("chests." + crateName + ".chest-regions", crateRegions);
            chestFile.config.set("chests." + crateName + ".chest-reward-amount", rewardAmount);
            chestFile.config.set("chests." + crateName + ".chest-glow", glowing);
            chestFile.config.set("chests." + crateName + ".chest-loot-to-spawn", lootToSpawn);
            chestFile.config.set("chests." + crateName + ".chest-animation-speed", speed.name());
            chestFile.config.set("chests." + crateName + ".chest-allows-pvp", allowsPvP);
            if(crate.getCrateLoot().size() == 0) {
                chestFile.save();
                return;
            }
            for(int i = 0; i < loot.size(); i++) {
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-item", loot.get(i).getLootItem());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-item-min", loot.get(i).getMin());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-item-max", loot.get(i).getMax());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-commands", loot.get(i).getLootCommands());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-chance", loot.get(i).getLootChance());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-hand-item", loot.get(i).getHandItem());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-holo", loot.get(i).getHandHolo());
                chestFile.config.set("chests." + crateName + ".loot." + i + ".loot-only-command", loot.get(i).isOnlyCommand());
                chestFile.save();
            }
            Logger.log("&aSaved Crate: " + crate.getName());
            Crate.crates.clear(); // remove all crates from list
        }
    }

}
