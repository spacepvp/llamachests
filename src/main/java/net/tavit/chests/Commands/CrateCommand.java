package net.tavit.chests.Commands;

import net.tavit.chests.Crate;
import net.tavit.chests.Enums.AnimationSpeed;
import net.tavit.chests.LlamaChests;
import net.tavit.chests.Loot;
import net.tavit.chests.Utilities.Logger;
import net.tavit.chests.Utilities.Messages;
import net.tavit.chests.Utilities.utils;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class CrateCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player) {
            final Player p = (Player) sender;

            /*
             * help message
             */
            if(args.length == 0) {
                p.sendMessage(Messages.CommandHelpMessage());
                return true;
            }

            if(args.length == 1) {

                /*
                 * help message
                 */
                if(args[0].equals("help")) {
                    p.sendMessage(Messages.CommandHelpMessage());
                    return true;
                }

                // invalid usage
                p.sendMessage(Messages.InvalidUsage(ChatColor.stripColor(args[0])));

                return true;
            }

            if(args.length == 2) {

                /*
                 * create command
                 */
                if(args[0].equals("create")) {

                    Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                    if(crate != null) {
                        p.sendMessage(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' already exists."));
                        return true;
                    }

                    Crate.crates.add(new Crate()
                    .setName(ChatColor.stripColor(args[1]))
                    .setCrateItem(new ItemStack(Material.ENDER_CHEST))
                    .setCrateItemName(Crate.DEFAULT_CRATE_NAME(ChatColor.stripColor(args[1])))
                    .setCrateItemLore(Crate.DEFAULT_CRATE_LORE())
                    .setGlowing(false)
                    .setRewardAmount(1)
                    .setLootToSpawn(15)
                    .setAnimationSpeed(AnimationSpeed.ONE)
                    .setLoot(new ArrayList<>()));

                    p.sendMessage(utils.color("&aCreated a new crate: " + ChatColor.stripColor(args[1])));

                    return true;
                }

                /*
                 * delete command
                 */
                if(args[0].equals("delete")) {

                    Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                    if(crate == null) {
                        p.sendMessage(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' does not exist."));
                        return true;
                    }

                    Crate.crates.remove(crate);
                    p.sendMessage(utils.color("&cDeleted crate: " + ChatColor.stripColor(args[1])));

                    return true;
                }

                /*
                 * get command
                 */
                if(args[0].equals("get")) {

                    Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                    if(crate == null) {
                        p.sendMessage(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' does not exist."));
                        return true;
                    }

                    p.getInventory().addItem(crate.getCrateItem());
                    p.sendMessage(utils.color("&7You got " + crate.getCrateItemName() + "&7!"));

                    return true;
                }

                // invalid usage
                p.sendMessage(Messages.InvalidUsage(ChatColor.stripColor(args[0] + " " + args[1])));

                return true;
            }

            if(args.length == 3) {

                if(args[0].equals("addloot")) {

                    Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                    if(crate == null) {
                        p.sendMessage(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' does not exist."));
                        return true;
                    }

                    String chance = args[2];

                    if(!NumberUtils.isNumber(chance)) {
                        p.sendMessage(utils.color("&cEnter a valid chance for loot."));
                        return true;
                    }

                    double chanceDouble = Double.parseDouble(chance);

                    ItemStack[] items = p.getInventory().getContents();

                    if(items.length == 0) {
                        p.sendMessage(utils.color("&cYou do not have any items in your inventory."));
                        return true;
                    }

                    for (ItemStack item : p.getInventory().getContents()) {
                        if(item == null) continue;
                        crate.getCrateLoot().add(new Loot()
                        .setMin(1)
                        .setMax(1)
                        .setLootItem(item)
                        .setHandHolo(item.hasItemMeta() ? item.getItemMeta().getDisplayName() : item.getType().name().replace("_", " "))
                        .setCommands(new ArrayList<>())
                        .setLootChance(chanceDouble)
                        .setHandItem(item)

                                .setIsOnlyCommand(false)
                        .build());
                        p.sendMessage(utils.color("&aAdded a new loot item to crate: " + crate.getName()));
                    }

                    return true;
                }

                return true;
            }

            if(args.length == 4) {

                if(args[0].equals("give")) {
                    Player target = Bukkit.getPlayer(args[1]);

                    if(target == null) {
                        p.sendMessage(utils.color("&c" + args[1] + " is not online."));
                        return true;
                    }

                    Crate crate = utils.getCrateByName(args[2]);

                    if(crate == null) {
                        p.sendMessage(utils.color("&cThis crate does not exist."));
                        return true;
                    }

                    String amount = args[3];

                    if(!NumberUtils.isNumber(amount)) {
                        p.sendMessage(utils.color("&cEnter a valid amount of crates to give."));
                        return true;
                    }

                    int amountInt = Integer.parseInt(amount);

                    target.getInventory().addItem(crate.getCrateItem(amountInt));
                    p.sendMessage(utils.color("&aGave " + target.getName() + " crate: " + crate.getName() + " x" + amountInt + "."));

                    return true;
                }

                return true;
            }

            return true;
        }

        /*
         * console command sender
         */

        if(args.length == 0) {
            Logger.log(Messages.CommandHelpMessage());
            return true;
        }

        if(args.length == 2) {

            /*
             * create command
             */
            if(args[0].equals("create")) {

                Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                if(crate != null) {
                    Logger.log(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' already exists."));
                    return true;
                }

                Crate.crates.add(new Crate()
                        .setName(ChatColor.stripColor(args[1]))
                        .setCrateItem(new ItemStack(Material.ENDER_CHEST))
                        .setCrateItemName(Crate.DEFAULT_CRATE_NAME(ChatColor.stripColor(args[1])))
                        .setCrateItemLore(Crate.DEFAULT_CRATE_LORE())
                        .setGlowing(false)
                        .setRewardAmount(1)
                        .setLootToSpawn(15)
                        .setAnimationSpeed(AnimationSpeed.ONE)
                        .setAllowsPvP(true)
                        .setLoot(new ArrayList<>()));

                Logger.log(utils.color("&aCreated a new crate: " + ChatColor.stripColor(args[1])));

                return true;
            }

            /*
             * delete command
             */
            if(args[0].equals("delete")) {

                Crate crate = utils.getCrateByName(ChatColor.stripColor(args[1]));

                if(crate == null) {
                    Logger.log(utils.color("&cCrate '" + ChatColor.stripColor(args[1]) + "' does not exist."));
                    return true;
                }

                Crate.crates.remove(crate);
                Logger.log(utils.color("&cDeleted crate: " + ChatColor.stripColor(args[1])));

                return true;
            }

            return true;
        }

        if(args.length == 4) {

            if(args[0].equals("give")) {
                Player target = Bukkit.getPlayer(args[1]);

                if(target == null) {
                    Logger.log(utils.color("&c" + args[1] + " is not online."));
                    return true;
                }

                Crate crate = utils.getCrateByName(args[2]);

                if(crate == null) {
                    Logger.log(utils.color("&cThis crate does not exist."));
                    return true;
                }

                String amount = args[3];

                if(!NumberUtils.isNumber(amount)) {
                    Logger.log(utils.color("&cEnter a valid amount of crates to give."));
                    return true;
                }

                int amountInt = Integer.parseInt(amount);

                target.getInventory().addItem(crate.getCrateItem(amountInt));
                Logger.log(utils.color("&aGave " + target.getName() + " crate: " + crate.getName() + " x" + amountInt + "."));

                return true;
            }

            return true;
        }

        return false;
    }
}
