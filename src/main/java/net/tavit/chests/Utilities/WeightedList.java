package net.tavit.chests.Utilities;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

public final class WeightedList<E> {
    private double totalWeight = 0.0;
    private final NavigableMap<Double, E> map = new TreeMap<Double, E>();

    public void add(double weight, E result) {
        if (weight <= 0.0) {
            return;
        }
        this.totalWeight += weight;
        this.map.put(this.totalWeight, result);
    }

    public E get() {
        double value = ThreadLocalRandom.current().nextDouble() * this.totalWeight;
        Map.Entry<Double, E> entry = this.map.higherEntry(value);
        return entry == null ? null : entry.getValue();
    }
}
