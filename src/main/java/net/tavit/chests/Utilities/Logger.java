package net.tavit.chests.Utilities;

import net.tavit.chests.LlamaChests;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {

    // use for log messages in console
    public static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&a[&b" +  LlamaChests.getPlugin(LlamaChests.class).getName() + "&a]&r " + msg);
        Bukkit.getConsoleSender().sendMessage(msg);
    }

    // use for debug messages in console
    public static void debug(String msg) {
        log("&7[&eDEBUG&7]&r" + msg);
    }
}
