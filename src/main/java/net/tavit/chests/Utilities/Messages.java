package net.tavit.chests.Utilities;

import net.tavit.chests.LlamaChests;

public class Messages {

    public static String CommandHelpMessage() {
        return utils.color(LlamaChests.getInstance().getConfig().getString("messages.help"));
    }

    public static String InvalidUsage(String invalid) {
        return utils.color("&cInvalid Usage: /crate " + invalid);
    }

    public static String noLoot() {
        return utils.color(LlamaChests.getInstance().getConfig().getString("messages.no-loot"));
    }

    public static String maxChestsOpening(int amount) {
        return utils.color(LlamaChests.getInstance().getConfig().getString("messages.max-chests-opening").replace("{amount}", Integer.toString(amount)));
    }

    public static String noPvP() {
        return utils.color(LlamaChests.getInstance().getConfig().getString("messages.no-pvp"));
    }

}
