package net.tavit.chests.Utilities;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.tavit.chests.Crate;
import net.tavit.chests.CrateOpening;
import net.tavit.chests.Loot;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.stream.Stream;

public class utils {

    public static String color(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    /**
     * @param loc location where sound is played
     * @param sound sound that is played
     * @param pitch pitch of sound
     */
    public static void playSound(Location loc, Sound sound, float pitch) {
        loc.getWorld().playSound(loc, sound, 1f, pitch);
    }

    /**
     * @param m material
     * @param amount amount of item
     * @param data used for color (example: wool:1(data) will be orange wool)
     * @param enchanted glow effect on item
     * @param name name of item
     * @param lore lore of item
     */
    public static ItemStack createItem(Material m, int amount, byte data, boolean enchanted, String name, String... lore) {
        ItemStack item = new ItemStack(m, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(org.bukkit.ChatColor.translateAlternateColorCodes('&', name));
        if (enchanted) {
            meta.addEnchant(Enchantment.getByName("DURABILITY"), 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        ArrayList<String> list = new ArrayList<>();
        for (String l : lore)
            list.add(org.bukkit.ChatColor.translateAlternateColorCodes('&', l));
        meta.setLore(list);
        item.setItemMeta(meta);
        return item;
    }

    /**
     * @param p player to get item from
     * @return craft itemstack from players hand
     */
    public static net.minecraft.server.v1_8_R3.ItemStack itemFromPlayerHand(Player p) {
        return CraftItemStack.asNMSCopy(p.getItemInHand());
    }

    /**
     * @param item item to turn into craft itemstack
     * @return craftitemstack
     */
    public static net.minecraft.server.v1_8_R3.ItemStack itemFromNMS(ItemStack item) {
        return CraftItemStack.asNMSCopy(item);
    }

    public static int getRandomNumber(int min, int max){
        return (int) ((Math.random()*((max-min)+1))+min);
    }

    public static int allowedChestsToOpen(Player p) {
        if(p.isOp()) return 1000;
        for(int i = 0; i < 1000; i++) {
            if(p.hasPermission("llamachests.open." + i)) {
                return i;
            }
        }
        return 1;
    }

    /**
     * @return amount of empty slots player has
     */
    public static int countEmptySlots(Player player) {
        return (int) Stream.of(player.getInventory().getContents()).filter(Objects::isNull).count();
    }

    /*
     * check if a location allows pvp
     */
    public static boolean allowsPVP(Location loc) {
        ApplicableRegionSet set = WGBukkit.getPlugin().getRegionManager(loc.getWorld()).getApplicableRegions(loc);
        return set.queryState(null, DefaultFlag.PVP) != StateFlag.State.DENY;
    }

    public static int chestOpeningsForPlayer(Player p) {
        if(CrateOpening.crateOpenings.size() == 0) return 0;
        int i = 0;
        for(CrateOpening openings : CrateOpening.crateOpenings) {
            if(openings.getPlayer() == p) i++;
        }
        return i;
    }

    /**
     * @param m material
     * @param amount amount of item
     * @param data used for color (example: wool:1(data) will be orange wool)
     * @param enchanted glow effect on item
     * @param name name of item
     * @param lore lore of item
     */
    public static net.minecraft.server.v1_8_R3.ItemStack createNMSItem(Material m, int amount, byte data, boolean enchanted, String name, String... lore) {
        ItemStack item = new ItemStack(m, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(color(name));
        ArrayList<String> itemLore = new ArrayList<>();
        for(String s : lore) {
            itemLore.add(color(s));
        }
        if(enchanted) {
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        meta.setLore(itemLore);
        item.setItemMeta(meta);
        return CraftItemStack.asNMSCopy(item);
    }

    public static boolean crateExists(String crate) {
        if(Crate.crates.size() == 0) return false;
        for(Crate crates : Crate.crates) {
            if(crates.getName().equals(crate)) return true;
        }
        return false;
    }

    public static Crate getCrateByName(String crate) {
        if(!crateExists(crate)) return null;
        for(Crate crates : Crate.crates) {
            if(crates.getName().equals(crate)) return crates;
        }
        return null;
    }

    public static boolean isCrateItem(ItemStack item) {
        if(Crate.crates.size() == 0) return true;
        for(Crate crates : Crate.crates) {
            return !item.isSimilar(crates.getCrateItem());
        }
        return true;
    }

    public static Crate getCrateByItem(ItemStack item) {
        if(Crate.crates.size() == 0) return null;
        for(Crate crates : Crate.crates) {
            return item.isSimilar(crates.getCrateItem()) ? crates : null;
        }
        return null;
    }

    public static String getRegion(Location l) {
        if (WorldGuardPlugin.inst() == null) {
            return null;
        }
        ApplicableRegionSet regions = WorldGuardPlugin.inst().getRegionManager(l.getWorld()).getApplicableRegions(l);
        if (regions == null) {
            return null;
        }
        Iterator<ProtectedRegion> iter = regions.iterator();
        ProtectedRegion reg = null;
        if (iter.hasNext()) {
            reg = iter.next();
            return reg.getId();
        }
        return null;
    }

    public static WeightedList<Loot> getWeightedLoot(Crate crate) {
        WeightedList<Loot> weightedTierLoot = new WeightedList<>();
        for(Loot loot : crate.getCrateLoot()) {
            weightedTierLoot.add(loot.getLootChance(), loot);
        }
        return weightedTierLoot;
    }

}
