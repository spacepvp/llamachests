package net.tavit.chests;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.*;
import net.tavit.chests.Enums.AnimationPhase;
import net.tavit.chests.Utilities.Logger;
import net.tavit.chests.Utilities.ParticleEffect;
import net.tavit.chests.Utilities.VectorUtils;
import net.tavit.chests.Utilities.utils;
import org.bukkit.*;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_8_R3.util.CraftMagicNumbers;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.EnderChest;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

public class CrateOpening implements Listener {

    public static List<CrateOpening> crateOpenings = new ArrayList<>();

    @Getter
    @Setter
    private Location
            startLocation, // location ender chest will start
            lootLocation; // location loot will spawn from

    @Getter
    @Setter
    private AnimationPhase animationPhase;

    @Getter
    @Setter
    private Crate crate;

    @Getter
    @Setter
    private Player player;

    @Getter
    @Setter
    private Map<EntityArmorStand, Loot> lootStands = new HashMap<>();

    @Getter
    @Setter
    private List<EntityArmorStand> holos = new ArrayList<>();

    @Getter
    @Setter
    private List<Integer> orbits = new ArrayList<>();

    @Getter
    @Setter
    private Map<Player, Integer> lootStage = new HashMap<>();

    @Getter
    @Setter
    private boolean isOrbit, readyToDangle, giveLoot;

    @Getter
    @Setter
    private float ENDER_ASCEND_ROTATION,
    ENDER_ASCEND_ASCEND,
    ENDER_ASCEND_TICK_1,
    ENDER_ASCEND_TICK_2,
    ENDER_ASCEND_TICK_3,
    SPAWN_LOOT_DELAY,
    ASCEND_LOOT_TICK_1,
    ASCEND_LOOT_TICK_2,
    ASCEND_LOOT_Y_PLUS,
    ASCEND_LOOT_TICK_3,
    ASCEND_LOOT_TICK_4,
    ORBIT_ARMORSTAND_TICK,
    REMOVE_ARMOR_STANDS;

    @Getter
    @Setter
    private BlockFace face;

    public CrateOpening(Player player, Crate crate, Location location, boolean airOpening, int SPEED) {
        /*
         * set speed values
         */
        if(SPEED == 1) {
            setENDER_ASCEND_ROTATION(20);
            setENDER_ASCEND_ASCEND(0.10f);
            setENDER_ASCEND_TICK_1(23);
            setENDER_ASCEND_TICK_2(27);
            setENDER_ASCEND_TICK_3(30);
            setSPAWN_LOOT_DELAY(10);
            setASCEND_LOOT_TICK_1(4);
            setASCEND_LOOT_TICK_2(3);
            setASCEND_LOOT_Y_PLUS(0.60f);
            setASCEND_LOOT_TICK_3(5);
            setASCEND_LOOT_TICK_4(5);
            setORBIT_ARMORSTAND_TICK(9);
            setREMOVE_ARMOR_STANDS(8);
        }
        if(SPEED == 2) {
            setENDER_ASCEND_ROTATION(20);
            setENDER_ASCEND_ASCEND(0.12f);
            setENDER_ASCEND_TICK_1(20);
            setENDER_ASCEND_TICK_2(24);
            setENDER_ASCEND_TICK_3(27);
            setSPAWN_LOOT_DELAY(7);
            setASCEND_LOOT_TICK_1(4);
            setASCEND_LOOT_TICK_2(3);
            setASCEND_LOOT_Y_PLUS(0.24f);
            setASCEND_LOOT_TICK_3(7);
            setASCEND_LOOT_TICK_4(8);
            setORBIT_ARMORSTAND_TICK(8);
            setREMOVE_ARMOR_STANDS(7);
        }
        if(SPEED == 3) {
            setENDER_ASCEND_ROTATION(20);
            setENDER_ASCEND_ASCEND(0.14f);
            setENDER_ASCEND_TICK_1(17);
            setENDER_ASCEND_TICK_2(21);
            setENDER_ASCEND_TICK_3(24);
            setSPAWN_LOOT_DELAY(5);
            setASCEND_LOOT_TICK_1(4);
            setASCEND_LOOT_TICK_2(3);
            setASCEND_LOOT_Y_PLUS(0.14f);
            setASCEND_LOOT_TICK_3(5);
            setASCEND_LOOT_TICK_4(5);
            setORBIT_ARMORSTAND_TICK(7);
            setREMOVE_ARMOR_STANDS(5);
        }
        /*
         * player opening crate
         */
        this.player = player;
        lootStage.put(player, 1);
        /*
         * orbit boolean, ready to dangle boolean, give loot boolean
         */
        isOrbit = false;
        readyToDangle = false;
        giveLoot = false;
        /*
         * animation phase
         */
        animationPhase = AnimationPhase.ENDER_ASCEND;
        /*
         * start location
         */
        startLocation = location;
        /*
         * loot spawn location
         */
        lootLocation = airOpening ? location.add(0, 3, 0) : location.add(0, 3, 0);
        /*
         * set crate for opening
         */
        this.crate = crate;
        /*
         * add crate opening to list
         */
        crateOpenings.add(this);
        /*
         * start ender animation
         */
        ENDER_ANIMATION();
        /*
         * start give loot task
         */
        GIVE_LOOT();
    }

    public boolean canOpen() {
        /*
         * crate opening doesnt contain this
         */
        if (!crateOpenings.contains(this)) return false;
        /*
         * player opening crate is dead
         */
        if (this.player.isDead()) return false;
        /*
         * player is not online
         */
        return this.player.isOnline();
    }

    private void GIVE_LOOT() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if(giveLoot) {
                    cancel();
                    giveLoot();
                    return;
                }
                if(!canOpen()) {
                    cancel();
                    try {
                        endChestOpening();
                    } catch (ArrayIndexOutOfBoundsException n) {
                        Logger.log("&eCrate tried ending but was already ended");
                    }
                }
            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    private void ENDER_ANIMATION() {
        /*
         * ender chest armor stand
         */
        WorldServer worldServer = ((CraftWorld) startLocation.getWorld()).getHandle();
        final EntityArmorStand entityArmorStand = new EntityArmorStand(worldServer);
        entityArmorStand.setGravity(false);
        entityArmorStand.setSmall(false);
        entityArmorStand.setInvisible(true);
        entityArmorStand.setArms(true);
        entityArmorStand.setBasePlate(false);
        entityArmorStand.setLocation(startLocation.getX() + 0.5,
                startLocation.getY() - 3.35, startLocation.getZ() + 0.5, 0, 0);
        // packet
        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(entityArmorStand);
        /*
         * send packet to all players
         */
        sendPacketToAllPlayers(packet);

        final Location teleportLocation = new Location(getArmorstandLocation(entityArmorStand).getWorld(),
                getArmorstandLocation(entityArmorStand).getX(),
                getArmorstandLocation(entityArmorStand).getY(),
                getArmorstandLocation(entityArmorStand).getZ());

        new BukkitRunnable() {

            int tick;
            int rotation;
            double ascend;

            @Override
            public void run() {
                tick++;
                rotation += ENDER_ASCEND_ROTATION;
                ascend += ENDER_ASCEND_ASCEND;

                if (!canOpen()) {
                    /*
                     * destroy entity packet
                     */
                    PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
                    sendPacketToAllPlayers(entityDestroy);
                    /*
                     * cancel bukkit task
                     */
                    cancel();
                    return;
                }

                if (tick == 1) {
                    /*
                     * equipment and data packets for ender chest
                     */
                    PacketPlayOutEntityEquipment equipmentPacket = new PacketPlayOutEntityEquipment(entityArmorStand.getId(), 4, CraftItemStack.asNMSCopy(new ItemStack(Material.ENDER_CHEST)));
                    PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata(entityArmorStand.getId(), entityArmorStand.getDataWatcher(), true);
                    /*
                     * send packets above to all players
                     */
                    sendPacketToAllPlayers(equipmentPacket);
                    sendPacketToAllPlayers(metaPacket);
                }

                /*
                 * armor stand teleport packet
                 */
                entityArmorStand.setLocation(teleportLocation.getX(), teleportLocation.getY() + ascend, teleportLocation.getZ(), rotation, 0);
                PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(entityArmorStand);

                /*
                 * send teleport packet to all players
                 */
                sendPacketToAllPlayers(teleportPacket);

                if (tick == ENDER_ASCEND_TICK_1) {
                    /*
                     * entity destroy packet
                     */
                    PacketPlayOutEntityDestroy entityDestroyPacket = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
                    /*
                     * remove ender chest armor stand
                     */
                    sendPacketToAllPlayers(entityDestroyPacket);
                    /*
                     * create ender chest facing player
                     */
                    setEnderChest();
                    face = BlockFace.valueOf(getDirectionPlayerIsFacing(player));
                }

                if (tick == ENDER_ASCEND_TICK_2) {
                    /*
                     * open ender chest
                     */
                    setEnderChestOpen(lootLocation.getBlock());
                    /*
                     * play particle and sound effect
                     */
                    lootLocation.getWorld().spigot().playEffect(lootLocation.add(0.5, 0, 0.5), Effect.LARGE_SMOKE, 0, 0, 0f, 0f, 0f, 0.05f, 35, 50);
                    lootLocation.getWorld().playSound(lootLocation, Sound.CHEST_OPEN, 1f, 2f);
                }

                if (tick == ENDER_ASCEND_TICK_3) {
                    cancel();
                    /*
                     * start loot orbit animation
                     */
                    SPAWN_LOOT();
                }

            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    private void SPAWN_LOOT() {

        /*
         * shuffle orbit paths
         */
        for (int i = 0; i < crate.getLootToSpawn(); i++) {
            orbits.add(i);
        }
        Collections.shuffle(orbits);

        new BukkitRunnable() {

            int tick;

            @Override
            public void run() {

                if (!canOpen()) {
                    cancel();
                    return;
                }

                if (tick < crate.getLootToSpawn()) {
                    /*
                     * play sound
                     */
                    utils.playSound(lootLocation, Sound.NOTE_BASS, 2f);
                    /*
                     * ascend loot
                     */
                    ASCEND_LOOT();
                }

                if (tick == crate.getLootToSpawn() + 2) {
                    /*
                     * remove armor stands
                     */
                    REMOVE_ARMOR_STANDS();
                    isOrbit = true;
                    cancel();
                }

                tick++;
            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, (long) SPAWN_LOOT_DELAY);
    }

    private void ASCEND_LOOT() {
        WorldServer worldServer = ((CraftWorld) lootLocation.getWorld()).getHandle();
        Location loc = lootLocation;
        final EntityArmorStand stand = new EntityArmorStand(worldServer);
        stand.setRightArmPose(new Vector3f(260, 318, 0));
        stand.setGravity(false);
        stand.setSmall(false);
        stand.setInvisible(true);
        stand.setArms(true);
        stand.setBasePlate(false);
        Location armorStandLocation = new Location(stand.getBukkitEntity().getWorld(), loc.getX(), loc.getY(), loc.getZ(), stand.yaw, stand.pitch);
        Location circleLocation = getLocationAroundCircle(armorStandLocation, 0.50, 1); // get circle location
        stand.setLocation(circleLocation.getX() - 0.05, circleLocation.getY() - 0.78, circleLocation.getZ() + 0.0, circleLocation.getYaw() - 14, circleLocation.getPitch());
        Loot randomLoot = utils.getWeightedLoot(crate).get();
        ItemStack handItem = randomLoot.getHandItem();
        stand.setCustomNameVisible(false);

        /*
         * add armor stand to loot list
         */
        lootStands.put(stand, randomLoot);

        final PacketPlayOutEntityEquipment equipmentPacket = new PacketPlayOutEntityEquipment(stand.getId(), 0, utils.itemFromNMS(handItem));
        final PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata(stand.getId(), stand.getDataWatcher(), true);
        final PacketPlayOutSpawnEntityLiving spawnEntityLivingPacket = new PacketPlayOutSpawnEntityLiving(stand); // make armor stand visible
        final PacketPlayOutEntityTeleport teleportSpawnPacket = new PacketPlayOutEntityTeleport(stand);

        sendPacketToAllPlayers(spawnEntityLivingPacket);
        sendPacketToAllPlayers(teleportSpawnPacket);

        new BukkitRunnable() {

            float tick;

            @Override
            public void run() {

                if (!canOpen()) {
                    cancel();
                    return;
                }

                if (tick == ASCEND_LOOT_TICK_1) {
                    sendPacketToAllPlayers(equipmentPacket);
                    sendPacketToAllPlayers(metaPacket);
                }

                if (tick > ASCEND_LOOT_TICK_2) {
                    stand.setLocation(stand.locX, stand.locY + ASCEND_LOOT_Y_PLUS, stand.locZ, stand.yaw, stand.pitch);
                }

                PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(stand);
                sendPacketToAllPlayers(teleportPacket);

                if (tick == ASCEND_LOOT_TICK_3) {
                    /*
                     * orbit armor stand
                     */
                    ORBIT_ARMORSTAND(stand, orbits.get(0));
                    /*
                     * remove orbit path
                     */
                    orbits.remove(0);
                }

                if (tick == ASCEND_LOOT_TICK_4) {
                    cancel();
                }

                tick++;
            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    /**
     * @param stand armor stand to animate
     * @param path  orbit path to take
     */
    private void ORBIT_ARMORSTAND(final EntityArmorStand stand, final int path) {

        animationPhase = AnimationPhase.LOOT_ORBIT;

        new BukkitRunnable() {

            float tick;

            @Override
            public void run() {

                if (!canOpen()) {
                    cancel();
                    return;
                }

                Location circleOrbit = getLocationAroundCircleOrbit(lootLocation, 0.50, tick / ORBIT_ARMORSTAND_TICK, path);
                stand.setLocation(circleOrbit.getX(), circleOrbit.getY(), circleOrbit.getZ(), stand.yaw, stand.pitch);

                PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(stand);
                sendPacketToAllPlayers(teleportPacket);
                //sendPacketToAllPlayers(teleportPacket);

                Location armorStandLocation = new Location(lootLocation.getWorld(), stand.locX, stand.locY, stand.locZ, stand.yaw, stand.pitch);
                Location circleLocation = getLocationAroundCircle(armorStandLocation, 0.50, tick / 3.6); // get circle location
                stand.setLocation(circleLocation.getX(), circleLocation.getY(), circleLocation.getZ(), circleLocation.getYaw(), circleLocation.getPitch());

                PacketPlayOutEntityTeleport circleTeleportPacket = new PacketPlayOutEntityTeleport(stand);
                sendPacketToAllPlayers(circleTeleportPacket);
                //sendPacketToAllPlayers(circleTeleportPacket);

                if (crate.getRewardAmount() == 1) {
                    if (isOrbit && lootStands.size() == 1 && readyToDangle) {
                        cancel();
                    }
                }

                if(crate.getRewardAmount() == 3) {
                    if (isOrbit && lootStands.size() == 3 && readyToDangle) {
                        cancel();
                    }
                }

                tick++;

            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    private void REMOVE_ARMOR_STANDS() {

        new BukkitRunnable() {
            @Override
            public void run() {

                if (!canOpen()) {
                    cancel();
                    return;
                }

                if(lootStands.size() == crate.getRewardAmount()) {
                    cancel();
                    lootLocation.getBlock().setType(Material.AIR);
                    lootLocation.getWorld().spigot().playEffect(lootLocation.add(0.5, 0, 0.5), Effect.LARGE_SMOKE, 0, 0, 0f, 0f, 0f, 0.05f, 35, 50);
                    readyToDangle = true;
                    for(int i = 0; i < crate.getRewardAmount(); i++) {
                        TELEPORT_ARMORSTAND_DANGLE(getLoot(lootStands, i + 1), i + 1);
                    }
                    new BukkitRunnable() {
                        int tick;

                        @Override
                        public void run() {
                            if (!canOpen()) {
                                cancel();
                            }
                            if (tick == 4) {
                                cancel();
                            }
                            tick++;
                            lootLocation.getWorld().playSound(lootLocation, Sound.NOTE_PLING, 0.5f, 2f);
                        }
                    }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 2L, 3L);
                    return;
                }

                removeRandomStand();

            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, (long) REMOVE_ARMOR_STANDS);
    }

    private void TELEPORT_ARMORSTAND_DANGLE(EntityArmorStand stand, int spot) {

        animationPhase = AnimationPhase.FINAL_ITEMS;
        lootStage.put(player, 2);

        Location current = new Location(stand.getBukkitEntity().getWorld(), stand.locX, stand.locY, stand.locZ, stand.yaw, stand.pitch);

        final EntityArmorStand holo = new EntityArmorStand(((CraftWorld) stand.getBukkitEntity().getWorld()).getHandle());
        holo.setGravity(false);
        holo.setSmall(true);
        holo.setInvisible(true);
        holo.setArms(true);
        holo.setBasePlate(false);
        holo.setLocation(current.getX(), current.getY(), current.getZ(), current.getYaw(), current.getPitch());
        holo.setCustomNameVisible(true);
        // final int randomAmount = utils.getRandomNumber(Loot.getByMetaString(chest, entityArmorStand.getCustomName().split(":")[0]).getMin(), Loot.getByMetaString(chest, entityArmorStand.getCustomName().split(":")[0]).getMax());
        holo.setCustomName(utils.color((lootStands.get(stand).getHandHolo() != null ? lootStands.get(stand).getHandHolo() : "No Hand Hologram") + " &7* &f" + lootStands.get(stand).getFinalChosenItem().getAmount()));
        PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(holo);
        PacketPlayOutEntityMetadata metaPacket = new PacketPlayOutEntityMetadata(holo.getId(), holo.getDataWatcher(), true);
        sendPacketToAllPlayers(packet);
        holos.add(holo);

        new BukkitRunnable() {
            float tick;
            float steps = 26;

            @Override
            public void run() {

                if (!canOpen()) {
                    cancel();
                    return;
                }

                if(tick == 4) {
                    sendPacketToAllPlayers(metaPacket);
                }

                stand.setLocation((float) current.getX(), (float) current.getY(), (float) current.getZ(), stand.yaw, stand.pitch);

                Vector adding = getTeleportSpot(player, spot).toVector();
                adding.subtract(current.toVector());
                adding.divide(new Vector(steps, steps, steps));
                current.add(adding);

                PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(stand);
                sendPacketToAllPlayers(teleportPacket);

                Location circleLocation = getLocationAroundCircle(current, 0.50, tick / 4); // get circle location
                Location holoCircleLocation = getLocationAroundCircle(current, 0, tick / 10); // get circle location
                stand.setLocation(circleLocation.getX(), circleLocation.getY(), circleLocation.getZ(), circleLocation.getYaw(), circleLocation.getPitch());
                holo.setLocation(holoCircleLocation.getX(), holoCircleLocation.getY() + (crate.getRewardAmount() != 1 && spot == 1 ? 1.75 : 1.15), holoCircleLocation.getZ(), holoCircleLocation.getYaw(), holoCircleLocation.getPitch());
                PacketPlayOutEntityTeleport entityTeleport = new PacketPlayOutEntityTeleport(stand);
                PacketPlayOutEntityTeleport holoTeleport = new PacketPlayOutEntityTeleport(holo);
                sendPacketToAllPlayers(entityTeleport);
                sendPacketToAllPlayers(entityTeleport);
                sendPacketToAllPlayers(holoTeleport);

                if (tick >= steps * 6) {
                    DANGLE_ITEM(stand, player, spot);
                    cancel();
                }

                tick++;
                steps--;
            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    private Location getTeleportSpot(Player p, int spot) {
        Location eyeLocation = p.getEyeLocation();

        // how many blocks to spawn the particles in front of the player
        double distanceFromEyes = 3.6;

        // how far to offset left/right from the center eye location
        double distanceFromEyeCenter = 1.75;

        Vector leftEye = VectorUtils.rotateVector(new Vector(distanceFromEyes - 0.25, -2, distanceFromEyeCenter), eyeLocation.getYaw(), eyeLocation.getPitch());
        Vector rightEye = VectorUtils.rotateVector(new Vector(distanceFromEyes - 0.25, -2, -distanceFromEyeCenter), eyeLocation.getYaw(), eyeLocation.getPitch());
        Vector centerEye = VectorUtils.rotateVector(new Vector(distanceFromEyes + 1, -2, 0), eyeLocation.getYaw(), eyeLocation.getPitch());

        Location leftLocation = eyeLocation.clone().add(leftEye);
        Location rightLocation = eyeLocation.clone().add(rightEye);
        Location teleportLocation = eyeLocation.clone().add(centerEye);

        if (spot == 3) {
            teleportLocation = leftLocation;
            // teleportLocationH = leftLocationH;
        } else if (spot == 2) {
            teleportLocation = rightLocation;
            // teleportLocationH = rightLocationH;
        }
        return teleportLocation;
    }

    private void DANGLE_ITEM(final EntityArmorStand entityArmorStand, final Player p, final int spot) {

        animationPhase = AnimationPhase.FINAL_ITEMS;

        new BukkitRunnable() {
            float tick;

            @Override
            public void run() {
                tick++;

                if (!canOpen()) {
                    cancel();
                    return;
                }

                entityArmorStand.setLocation(getTeleportSpot(p, spot).getX(), getTeleportSpot(p, spot).getY(), getTeleportSpot(p, spot).getZ(), entityArmorStand.yaw, entityArmorStand.pitch);
                // Spawn particles at leftLocation and rightLocation and they will
                // appear in front of the player and offset to the left and right
                PacketPlayOutEntityTeleport teleportPacket = new PacketPlayOutEntityTeleport(entityArmorStand);
                sendPacketToAllPlayers(teleportPacket);

                Location armorStandLocation = new Location(entityArmorStand.getBukkitEntity().getWorld(), entityArmorStand.locX, entityArmorStand.locY, entityArmorStand.locZ, entityArmorStand.yaw, entityArmorStand.pitch);
                Location circleLocation = getLocationAroundCircle(armorStandLocation, 0.50, tick / 4); // get circle location
                Location holoCircleLocation = getLocationAroundCircle(armorStandLocation, 0, tick / 4); // get circle location
                entityArmorStand.setLocation(circleLocation.getX(), circleLocation.getY(), circleLocation.getZ(), circleLocation.getYaw(), circleLocation.getPitch());
                holos.get(spot - 1).setLocation(holoCircleLocation.getX(), holoCircleLocation.getY() + (crate.getRewardAmount() != 1 && spot == 1 ? 1.75 : 1.15), holoCircleLocation.getZ(), holoCircleLocation.getYaw(), holoCircleLocation.getPitch());
                PacketPlayOutEntityTeleport entityTeleport = new PacketPlayOutEntityTeleport(entityArmorStand);
                PacketPlayOutEntityTeleport holoTeleport = new PacketPlayOutEntityTeleport(holos.get(spot - 1));
                sendPacketToAllPlayers(entityTeleport);
                sendPacketToAllPlayers(entityTeleport);
                sendPacketToAllPlayers(holoTeleport);

                if (tick == 20 * 5) {
                    cancel();
                    giveLoot = true;
                    getArmTip(entityArmorStand);
                    PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(entityArmorStand.getId());
                    sendPacketToAllPlayers(entityDestroy);
                    PacketPlayOutEntityDestroy destroyHolo = new PacketPlayOutEntityDestroy(holos.get(spot - 1).getId());
                    sendPacketToAllPlayers(destroyHolo);
                }
            }
        }.runTaskTimerAsynchronously(LlamaChests.getInstance(), 0L, 1L);
    }

    @EventHandler
    private void onLeave(PlayerQuitEvent e) {
        final Player p = e.getPlayer();
        if (crateOpenings.size() == 0) return;
        for (CrateOpening openings : crateOpenings) {
            if (openings.player == p) {
                openings.giveLootLeave();
            }
        }
    }

    @EventHandler
    private void onPickup(PlayerPickupItemEvent e) {
        final Player p = e.getPlayer();
        if(crateOpenings.size() == 0) return;
        if(utils.chestOpeningsForPlayer(p) > 0) e.setCancelled(true);
    }

    private void giveLoot() {
        for(Map.Entry<EntityArmorStand, Loot> loot : lootStands.entrySet()) {
            if (loot.getValue().isOnlyCommand()) {
                if (loot.getValue().getLootCommands().size() != 0) {
                    for (String commands : loot.getValue().getLootCommands()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), commands.replace("{amount}", Integer.toString(loot.getValue().getFinalChosenItem().getAmount())).replace("{player}", player.getName()));
                    }
                }
                return;
            }
            if (loot.getValue().getLootCommands().size() != 0) {
                for (String commands : loot.getValue().getLootCommands()) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), commands.replace("{amount}", Integer.toString(loot.getValue().getFinalChosenItem().getAmount())).replace("{player}", player.getName()));
                }
            }
            player.getInventory().addItem(loot.getValue().getFinalChosenItem());
        }
        crateOpenings.remove(this);
    }

    private void giveLootLeave() {
        if(lootStage.get(player) == 1) {
            player.getInventory().addItem(crate.getCrateItem());
            return;
        }
        for(Map.Entry<EntityArmorStand, Loot> loot : lootStands.entrySet()) {
            if (loot.getValue().isOnlyCommand()) {
                if (loot.getValue().getLootCommands().size() != 0) {
                    for (String commands : loot.getValue().getLootCommands()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), commands.replace("{amount}", Integer.toString(loot.getValue().getFinalChosenItem().getAmount())).replace("{player}", player.getName()));
                    }
                }
                return;
            }
            if (loot.getValue().getLootCommands().size() != 0) {
                for (String commands : loot.getValue().getLootCommands()) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), commands.replace("{amount}", Integer.toString(loot.getValue().getFinalChosenItem().getAmount())).replace("{player}", player.getName()));
                }
            }
            player.getInventory().addItem(loot.getValue().getFinalChosenItem());
        }
        lootStage.clear();
    }

    private void endChestOpening() {
        lootLocation.getBlock().setType(Material.AIR);
        if(lootStands.size() != 0) {
            for(Map.Entry<EntityArmorStand, Loot> stands : lootStands.entrySet()) {
                PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(stands.getKey().getId());
                sendPacketToAllPlayers(entityDestroy);
            }
        }
        lootStands.clear();
        if(holos.size() != 0) {
            for(EntityArmorStand stands : holos) {
                PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(stands.getId());
                sendPacketToAllPlayers(entityDestroy);
            }
        }
        holos.clear();
        orbits.clear();
        crateOpenings.remove(this);
    }

    private void removeRandomStand() {
        EntityArmorStand stand = randomItem(lootStands);
        lootStands.remove(stand);
        getArmTip(stand);
        utils.playSound(lootLocation, Sound.CHICKEN_EGG_POP, 2f);
        PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(stand.getId());
        sendPacketToAllPlayers(entityDestroy);
    }

    private void sendPacketToAllPlayers(net.minecraft.server.v1_8_R3.Packet<?> packet) {
        for (Player players : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) players).getHandle().playerConnection.sendPacket(packet);
        }
    }

    public static Location getArmTip(EntityArmorStand as) {
        // Gets shoulder location
        Location location = new Location(as.getBukkitEntity().getWorld(), as.locX, as.locY, as.locZ, as.yaw, as.pitch);
        Location asl = location.clone();
        asl.setYaw(asl.getYaw() + 90f);
        Vector dir = asl.getDirection();
        asl.setX(asl.getX() + 5f / 16f * dir.getX());
        asl.setY(asl.getY() + 22f / 16f);
        asl.setZ(asl.getZ() + 5f / 16f * dir.getZ());
        // asl.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, asl, 1);
        // Get Hand Location

        Vector3f ea = as.rightArmPose;
        Vector armDir = getDirection(ea.getX(), ea.getX(), -ea.getZ());
        armDir = rotateAroundAxisY(armDir, Math.toRadians(asl.getYaw() - 90f));
        asl.setX(asl.getX() + 10f / 16f * armDir.getX());
        asl.setY(asl.getY() + 10f / 16f * armDir.getY());
        asl.setZ(asl.getZ() + 10f / 16f * armDir.getZ());

        // asl.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, asl, 1);
        ParticleEffect.SMOKE_LARGE.display(0, 0, 0, 0.01f, 4,
                asl, 15);
        return asl;
    }

    public static Vector getDirection(float yaw, float pitch, float roll) {
        Vector v = new Vector(0, -1, 0);
        v = rotateAroundAxisX(v, pitch);
        v = rotateAroundAxisY(v, yaw);
        v = rotateAroundAxisZ(v, roll);
        return v;
    }

    private static Vector rotateAroundAxisX(Vector v, double angle) {
        double y, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        y = v.getY() * cos - v.getZ() * sin;
        z = v.getY() * sin + v.getZ() * cos;
        return v.setY(y).setZ(z);
    }

    private static Vector rotateAroundAxisY(Vector v, double angle) {
        angle = -angle;
        double x, z, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos + v.getZ() * sin;
        z = v.getX() * -sin + v.getZ() * cos;
        return v.setX(x).setZ(z);
    }

    private static Vector rotateAroundAxisZ(Vector v, double angle) {
        double x, y, cos, sin;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
        x = v.getX() * cos - v.getY() * sin;
        y = v.getX() * sin + v.getY() * cos;
        return v.setX(x).setY(y);
    }

    private void setEnderChest() {
        /*
         * set ender chest block at location
         */
        lootLocation.getBlock().setType(Material.ENDER_CHEST); // set block to ender chest
        /*
         * get ender chest block
         */
        org.bukkit.block.Block block = lootLocation.getBlock(); // get block at ender chest location
        /*
         * create a new ender chest
         */
        EnderChest enderChest = new EnderChest(BlockFace.valueOf(getDirectionPlayerIsFacing(player))); // create a new ender chest
        /*
         * get block state of ender chest
         */
        BlockState state = block.getState(); // get the block state of the ender chest
        /*
         * set ender chest data
         */
        state.setData(enderChest); // set ender chest to open
        /*
         * update ender chest block state
         */
        state.update(); // update the ender chest so it shows the ender chest opening
    }

    private void setEnderChestOpen(Block chest) {
        Location loc = chest.getLocation();
        ((CraftWorld) loc.getWorld()).getHandle().playBlockAction(new BlockPosition(chest.getX(), chest.getY(), chest.getZ()), CraftMagicNumbers.getBlock(chest), 1, 1);
    }

    private String getDirectionPlayerIsFacing(Player player) {
        float yaw = player.getLocation().getYaw();
        if (yaw < 0) {
            yaw += 360;
        }
        if (yaw >= 315 || yaw < 45) {
            return "NORTH";
        } else if (yaw < 135) {
            return "EAST";
        } else if (yaw < 225) {
            return "SOUTH";
        } else if (yaw < 315) {
            return "WEST";
        }
        return "NORTH";
    }

    private Location getArmorstandLocation(EntityArmorStand stand) {
        return new Location(startLocation.getWorld(), stand.locX, stand.locY, stand.locZ, stand.yaw, stand.pitch);
    }

    private EntityArmorStand randomItem(Map<EntityArmorStand, Loot> map) {
        Set<EntityArmorStand> keySet = map.keySet();
        List<EntityArmorStand> keyList = new ArrayList<>(keySet);

        int size = keyList.size();
        int randIdx = new Random().nextInt(size);

        return keyList.get(randIdx);
    }

    private EntityArmorStand getLoot(Map<EntityArmorStand, Loot> map, int spot) {
        Set<EntityArmorStand> keySet = map.keySet();
        List<EntityArmorStand> keyList = new ArrayList<>(keySet);

        return keyList.get(spot - 1);
    }

    public Location getLocationAroundCircle(Location center, double radius, double angleInRadian) {
        double x = center.getX() + radius * Math.cos(angleInRadian);
        double z = center.getZ() + radius * Math.sin(angleInRadian);
        double y = center.getY();

        Location loc = new Location(center.getWorld(), x, y, z);
        Vector difference = center.toVector().clone().subtract(loc.toVector()); // this sets the returned location's direction toward the center of the circle
        loc.setDirection(difference);

        return loc;
    }

    private Location getLocationAroundCircleOrbit(Location center, double radius, double angleInRadian, int tickDirection) {
        double x = 0;
        double z = 0;
        double y = 0;

        if (tickDirection == 0 || tickDirection == 16) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 1) {
            x = center.getX();
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 2) {
            x = center.getX() - radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 3) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ();
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 4) {
            x = center.getX() - radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() - radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 5 || tickDirection == 17) {
            x = center.getX();
            z = center.getZ() - radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 6) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() - radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 7 || tickDirection == 19) {
            x = center.getX() - radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ();
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        if (tickDirection == 8) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 1
        if (tickDirection == 9) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 2
        if (tickDirection == 10 || tickDirection == 18) {
            x = center.getX();
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 3
        if (tickDirection == 11) {
            x = center.getX() - radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() + radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 4
        if (tickDirection == 12) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ();
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 5
        if (tickDirection == 13 || tickDirection == 20) {
            x = center.getX() - radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ() - radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 6
        if (tickDirection == 14) {
            x = center.getX();
            z = center.getZ() - radius * Math.sin(angleInRadian) * 2.5;
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        // 7
        if (tickDirection == 15) {
            x = center.getX() + radius * Math.sin(angleInRadian) * 2.5;
            z = center.getZ();
            y = center.getY() - 1 + radius * Math.cos(angleInRadian) * 2.5;
        }

        return new Location(center.getWorld(), x, y, z);
    }

    public CrateOpening() {
    }

}
