package net.tavit.chests;

import lombok.Getter;
import lombok.Setter;
import net.tavit.chests.Enums.AnimationSpeed;
import net.tavit.chests.Utilities.utils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Crate {

    public static List<Crate> crates = new ArrayList<>();

    @Getter
    private String name;

    /*
     * settings
     */
    @Getter
    private int rewardAmount;

    /*
     * crate item
     */
    private ItemStack crateItem;

    @Getter
    private String crateItemName;

    @Getter
    private ArrayList<String>
            crateItemLore,
            allowedWorlds,
            allowedRegions;

    @Getter
    private boolean glowing, allowsPvP;

    @Getter
    private int lootToSpawn;

    @Getter
    private AnimationSpeed animationSpeed;

    /*

            Block targetBlock = player.getTargetBlock((Set<Material>) null, range);
            get target block in specific range. use when player doesnt place crate

     */



    /*
     * crate loot
     */
    @Getter
    private List<Loot> crateLoot;

    public Crate setName(String name) {
        this.name = name;
        return this;
    }

    public Crate setRewardAmount(int amount) {
        this.rewardAmount = amount;
        return this;
    }

    public Crate setCrateItem(ItemStack item) {
        this.crateItem = item;
        return this;
    }

    public Crate setCrateItemName(String name) {
        this.crateItemName = name;
        return this;
    }

    public Crate setCrateItemLore(ArrayList<String> lore) {
        this.crateItemLore = lore;
        return this;
    }

    public Crate setCrateWorlds(ArrayList<String> worlds) {
        this.allowedWorlds = worlds;
        return this;
    }

    public Crate setCrateRegions(ArrayList<String> regions) {
        this.allowedRegions = regions;
        return this;
    }

    public Crate setGlowing(boolean b) {
        this.glowing = b;
        return this;
    }

    public Crate setLoot(List<Loot> loot) {
        this.crateLoot = loot;
        return this;
    }

    public Crate setLootToSpawn(int loot) {
        this.lootToSpawn = loot;
        return this;
    }

    public Crate setAnimationSpeed(AnimationSpeed speed) {
        this.animationSpeed = speed;
        return this;
    }

    public Crate setAllowsPvP(boolean b) {
        this.allowsPvP = b;
        return this;
    }

    public Crate build() {
        Crate crate = new Crate();
        crate.name = this.name;
        crate.rewardAmount = this.rewardAmount;
        crate.crateItem = this.crateItem;
        crate.crateItemName = this.crateItemName;
        crate.crateItemLore = this.crateItemLore;
        crate.glowing = this.glowing;
        crate.crateLoot = this.crateLoot;
        crate.lootToSpawn = this.lootToSpawn;
        crate.animationSpeed = this.animationSpeed;
        crate.allowsPvP = this.allowsPvP;
        return crate;
    }

    public ItemStack getCrateItem(int amount) {
        ItemStack item = new ItemStack(this.crateItem.getType());
        item.setAmount(amount);
        ItemMeta meta = item.getItemMeta();
        item.setDurability(this.crateItem.getDurability());
        if(this.glowing) {
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        meta.setDisplayName(utils.color(this.crateItemName));
        ArrayList<String> lore = new ArrayList<>();
        for(String s : this.crateItemLore) {
            lore.add(utils.color(s));
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    public ItemStack getCrateItem() {
        ItemStack item = new ItemStack(this.crateItem.getType());
        ItemMeta meta = item.getItemMeta();
        item.setDurability(this.crateItem.getDurability());
        if(this.glowing) {
            meta.addEnchant(Enchantment.DURABILITY, 1, true);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        meta.setDisplayName(utils.color(this.crateItemName));
        ArrayList<String> lore = new ArrayList<>();
        for(String s : this.crateItemLore) {
            lore.add(utils.color(s));
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    public static String DEFAULT_CRATE_NAME(String crate) {
        return "&f&l%crate% Crate &7(Place)".replace("%crate%", crate);
    }

    public static ArrayList<String> DEFAULT_CRATE_LORE() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add(utils.color("&7A cache of mystery equipment packaged"));
        lore.add(utils.color("&7by the Intergalactic Space Station!"));
        lore.add("");
        lore.add(utils.color("&cEnsure you have enough inventory space!"));
        lore.add(utils.color("&d&nDo in an open area for best results!"));
        lore.add(utils.color("&7Hint: Place on ground"));
        return lore;
    }

}
