package net.tavit.chests.Enums;

import lombok.Getter;
import net.tavit.chests.Utilities.utils;

public enum AnimationSpeed {

    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(utils.getRandomNumber(1, 3));

    @Getter
    int speed;

    AnimationSpeed(int speed) {
        this.speed = speed;
    }

}
