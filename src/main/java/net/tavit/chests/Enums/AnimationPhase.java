package net.tavit.chests.Enums;

public enum AnimationPhase {
    ENDER_ASCEND, LOOT_ORBIT, FINAL_ITEMS;
}
