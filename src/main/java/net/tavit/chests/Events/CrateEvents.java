package net.tavit.chests.Events;

import net.tavit.chests.Crate;
import net.tavit.chests.CrateOpening;
import net.tavit.chests.Utilities.Messages;
import net.tavit.chests.Utilities.utils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

public class CrateEvents implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        final Player p = e.getPlayer();
        final ItemStack item = p.getItemInHand();
        final Action action = e.getAction();

        /*
         * RIGHT CLICK AIR
         */
        if(action == Action.RIGHT_CLICK_AIR) {
            // not holding item
            if (item == null) return;
            // no crates exist
            if (Crate.crates.size() == 0) return;
            // item is not a crate item
            if (utils.isCrateItem(item)) return;

            Crate crate = utils.getCrateByItem(item);
            e.setCancelled(true);

            if(p.isFlying() || p.getLocation().getBlock().isLiquid() || p.getLocation().add(0, -1, 0).getBlock().getType() == Material.AIR) {
                p.sendMessage(utils.color("&cYou must be on the ground to open this crate."));
                return;
            }

            Block targetBlock = p.getTargetBlock((Set<Material>) null, 4);
            Location loc = targetBlock.getLocation();
            loc.setY(p.getLocation().getY() - 1);

            if(crate.getCrateLoot().size() == 0) {
                p.sendMessage(Messages.noLoot());
            }

            if(!crate.isAllowsPvP() && utils.allowsPVP(p.getLocation())) {
                p.sendMessage(Messages.noPvP());
                return;
            }

            if (loc.add(0, 2, 0).getBlock().getType() != Material.AIR) {
                p.sendMessage(utils.color("&c&l(!) &cYou must open this space chest in an open area."));
                return;
            }

            if(crate.getLootToSpawn() == 0 || crate.getLootToSpawn() < 1 && crate.getRewardAmount() == 1 || crate.getLootToSpawn() < 3 && crate.getRewardAmount() == 3) {
                p.sendMessage(utils.color("&cCan not open crate. Loot spawn amount is less than reward amount."));
                return;
            }

            if(crate.getRewardAmount() != 1 && crate.getRewardAmount() != 3) {
                p.sendMessage(utils.color("&cCan not open crate. Reward amount must be 1 OR 3."));
                return;
            }

            if(utils.chestOpeningsForPlayer(p) + 1 > utils.allowedChestsToOpen(p)) {
                p.sendMessage(Messages.maxChestsOpening(utils.chestOpeningsForPlayer(p)));
                return;
            }

            if(!canOpenCrate(crate, p)) {
                p.sendMessage(utils.color("&cYou do not have enough inventory space to open this crate."));
                return;
            }

            new CrateOpening(p, crate, loc.add(0, -2, 0), true, crate.getAnimationSpeed().getSpeed());
            if(p.getItemInHand().getAmount() - 1 > 0) {
                p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
            } else {
                p.setItemInHand(null);
            }

            return;
        }

        /*
         * RIGHT CLICK BLOCK
         */
        if(action == Action.RIGHT_CLICK_BLOCK) {
            // not holding item
            if (item == null) return;
            // no crates exist
            if (Crate.crates.size() == 0) return;
            // item is not a crate item
            if (utils.isCrateItem(item)) return;

            Crate crate = utils.getCrateByItem(item);
            e.setCancelled(true);

            if(crate.getCrateLoot().size() == 0) {
                p.sendMessage(Messages.noLoot());
            }

            if(!crate.isAllowsPvP() && utils.allowsPVP(p.getLocation())) {
                p.sendMessage(Messages.noPvP());
                return;
            }

            if (e.getClickedBlock().getLocation().add(0, 2, 0).getBlock().getType() != Material.AIR) {
                p.sendMessage(utils.color("&c&l(!) &cYou must open this space chest in an open area."));
                return;
            }

            if(crate.getLootToSpawn() == 0 || crate.getLootToSpawn() < 1 && crate.getRewardAmount() == 1 || crate.getLootToSpawn() < 3 && crate.getRewardAmount() == 3) {
                p.sendMessage(utils.color("&cCan not open crate. Loot spawn amount is less than reward amount."));
                return;
            }

            if(crate.getRewardAmount() != 1 && crate.getRewardAmount() != 3) {
                p.sendMessage(utils.color("&cCan not open crate. Reward amount must be 1 OR 3."));
                return;
            }

            if(utils.chestOpeningsForPlayer(p) + 1 > utils.allowedChestsToOpen(p)) {
                p.sendMessage(Messages.maxChestsOpening(utils.chestOpeningsForPlayer(p)));
                return;
            }

            if(!canOpenCrate(crate, p)) {
                p.sendMessage(utils.color("&cYou do not have enough inventory space to open this crate."));
                return;
            }

            new CrateOpening(p, crate, e.getClickedBlock().getLocation(), false, crate.getAnimationSpeed().getSpeed());
            if(p.getItemInHand().getAmount() - 1 > 0) {
                p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
            } else {
                p.setItemInHand(null);
            }
        }

    }

    public boolean canOpenCrate(Crate crate, Player p) {
        if(CrateOpening.crateOpenings.size() == 0) {
            int rewardAmount = crate.getRewardAmount();
            if(utils.countEmptySlots(p) < rewardAmount) {
                return false;
            }
        }
        int i = crate.getRewardAmount();
        for(CrateOpening opening : CrateOpening.crateOpenings) {
            if(opening.getPlayer() == p) {
                i += opening.getCrate().getRewardAmount();
            }
        }
        return utils.countEmptySlots(p) >= i;
    }

}
